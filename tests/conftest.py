#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import uuid
import pytest
from datetime import datetime

from assets_db.model import Assets, Sync
from assets_db.utils import create_db, create_session


def get_dir_path():
    return os.path.dirname(os.path.abspath(__file__))


def get_db_path():
    return os.path.join(get_dir_path(), 'data', 'test.db')


def get_sqlite():
    return 'sqlite:///' + get_db_path()


@pytest.fixture()
def dir_path():
    return get_dir_path()


@pytest.fixture()
def db_url():
    return get_sqlite()


@pytest.fixture()
def db_path():
    return get_db_path()


@pytest.fixture()
def cfg_path():
    return os.path.join(get_dir_path(), 'data', 'test_settings.cfg')


@pytest.fixture(scope='module')
def setup_db(request):
    create_db(url=get_sqlite())

    def delete():
        os.remove(get_db_path())
    request.addfinalizer(delete)


@pytest.fixture()
def insert(request):
    rec = Assets(identifier='id1', dataset_puid=str(uuid.uuid4()), asset_id='a1', from_blockchain=False,
                 date_modified= datetime.now())
    s = create_session(get_sqlite())
    s.add(rec)
    s.commit()

    def delete():
        s.query(Assets).filter(Assets.identifier == 'id1').delete()
        s.commit()
        s.close()
    request.addfinalizer(delete)

    return s.query(Assets).filter(Assets.identifier == 'id1').one()


@pytest.fixture()
def session(request):
    session = create_session(get_sqlite())

    def end_session():
        session.query(Assets).delete()
        session.query(Sync).delete()
        session.commit()
        session.close()
    request.addfinalizer(end_session)

    return session


@pytest.fixture()
def idict():
    return [{
        'identifier': 'id1',
        'dataset_puid': str(uuid.uuid4()),
        'asset_id': 'a1',
        'from_blockchain': False,
        'date_modified': datetime.now(),
    }, {
        'identifier': 'id2',
        'dataset_puid': str(uuid.uuid4()),
        'asset_id': 'a2',
        'from_blockchain': False,
        'date_modified': datetime.now(),
    }]
