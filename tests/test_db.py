import os
from datetime import date
from assets_db.query import Assets, Sync
from assets_db.utils import create_db, get_url
from assets_db.query import get_asset, get_missing_identifiers, get_synced_dates
from assets_db.insert import insert_dicts, insert_sync_dates


def test_create_db(dir_path):
    db_path = os.path.join(dir_path, 'data', 'create.db')
    assert not os.path.isfile(db_path)
    create_db(url='sqlite:///' + db_path)
    assert os.path.isfile(db_path)
    os.remove(db_path)


def test_get_assets(setup_db, insert, db_url):
    _, asset_id, from_blockchain = get_asset(insert.identifier, blockchain_only=False, url=db_url)
    assert asset_id == insert.asset_id
    assert not from_blockchain


def test_get_assets_blockchain(setup_db, session, db_url, idict):
    idict[0]['from_blockchain'] = True
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 0
    insert_dicts(idict, url=db_url)
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 2
    asset = get_asset(idict[0]['identifier'], blockchain_only=True, url=db_url)
    assert asset is not None
    _, _, from_blockchain = asset
    assert from_blockchain

    asset = get_asset(idict[0]['identifier'], blockchain_only=False, url=db_url)
    assert asset is not None
    _, _, from_blockchain = asset
    assert from_blockchain

    asset = get_asset(idict[1]['identifier'], blockchain_only=False, url=db_url)
    assert asset is not None
    _, _, from_blockchain = asset
    assert not from_blockchain

    asset = get_asset(idict[1]['identifier'], blockchain_only=True, url=db_url)
    assert asset is None


def test_get_url(db_url, cfg_path):
    assert get_url(url=db_url)

    os.environ['DMA_ASSETS_DB_URL'] = 'my_test'
    assert get_url() == 'my_test'
    os.environ.pop('DMA_ASSETS_DB_URL')

    assert get_url(conf_path=cfg_path) == 'sqlite:///test_cfg.db'

    os.environ['DMA_ASSETS_DB_CONFIG_PATH'] = cfg_path
    assert get_url() == 'sqlite:///test_cfg.db'
    os.environ.pop('DMA_ASSETS_DB_CONFIG_PATH')


def test_missing_identifiers(setup_db, db_url, session, idict):
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 0
    insert_dicts(idict, url=db_url)
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 2
    identifiers_to_check = ['id1', 'id2', 'id3']
    assert list(get_missing_identifiers(identifiers_to_check, blockchain_only=False, url=db_url).keys()) == ['id3']


def test_missing_identifiers_blockchain(setup_db, db_url, session, idict):
    idict[0]['from_blockchain'] = True
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 0
    insert_dicts(idict, url=db_url)
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 2

    identifiers_to_check = ['id1', 'id2', 'id3']
    missing_both = get_missing_identifiers(identifiers_to_check, blockchain_only=False, url=db_url)
    missing_blockchain = get_missing_identifiers(identifiers_to_check, blockchain_only=True, url=db_url)
    assert missing_both == {'id3': 'created'}
    assert missing_blockchain == {'id2': 'updated', 'id3': 'created'}


def test_completely_sync_dates(setup_db, db_url, session):
    in_date = date(2018, 2, 3)
    insert_sync_dates(in_date, url=db_url)
    assert len(session.query(Sync).all()) == 1
    assert get_synced_dates(url=db_url) == [in_date]
