import uuid
from datetime import date, datetime, timedelta

from assets_db.model import Assets, Sync
from assets_db.insert import insert_dicts, insert_sync_dates, insert_metadata_asset
from assets_db.utils import MetadataAsset


def test_insert_dicts(setup_db, db_url, session, idict):
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 0
    insert_dicts(idict, url=db_url)
    assert len(session.query(Assets).filter(Assets.identifier.in_(['id1', 'id2'])).all()) == 2


def test_insert_metadata_asset(setup_db, db_url, session):
    d = datetime(2017, 1, 1, 2, 2, 2)
    md = [MetadataAsset('id' + str(n), d + timedelta(days=n)) for n in range(3)]
    for m in md:
        m.set_asset(asset_id=str(uuid.uuid4()), from_blockchain=False)

    assert len(session.query(Assets).all()) == 0
    insert_metadata_asset(md, url=db_url)
    assert len(session.query(Assets).all()) == 3


def test_insert_sync(setup_db, db_url, session):
    assert len(session.query(Sync).all()) == 0
    insert_sync_dates(date(2018, 1, 2), url=db_url)
    assert len(session.query(Sync).all()) == 1
