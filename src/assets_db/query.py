import logging
from datetime import date
from sqlalchemy import func, or_, and_, distinct

from assets_db.model import Assets, Sync
from assets_db.utils import create_session, get_url

_logger = logging.getLogger(__name__)


def get_asset(identifier: str, blockchain_only: bool=True, conf_path: str=None, url: str=None) -> str:
    """
    Get the corresponding asset to an identifier (if identifier exists).
    :param identifier: [str] identifier for which the asset is needed
    :param blockchain_only: [bool] whether only Asset IDs which from the blockchain are considered (default, True)
    :param conf_path: [str] path to a config file
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return: [str] asset
    """
    url = get_url(conf_path, url)
    session = create_session(url)

    if blockchain_only:
        query = session.query(Assets.dataset_puid, Assets.asset_id, Assets.from_blockchain) \
            .filter(Assets.identifier == identifier)\
            .filter(Assets.from_blockchain)
    else:
        l = session.query(Assets.identifier, func.max(Assets.date_modified).label('latest_date'))\
            .filter(Assets.identifier == identifier)\
            .group_by(Assets.identifier)\
            .subquery('l')
        query = session.query(Assets.dataset_puid, Assets.asset_id, Assets.from_blockchain) \
            .filter(Assets.identifier == l.c.identifier)\
            .filter(Assets.date_modified == l.c.latest_date)
    return query.one_or_none()


def get_missing_identifiers(identifiers: list, blockchain_only: bool=True, conf_path: str=None, url: str=None) -> dict:
    """
    Return list of in db missing identifiers from list.
    :param identifiers: [list] list of identifiers to check
    :param blockchain_only: [bool] whether Assets retrieved from the blockchain (default, True) or both Assers from the
    blockchain and randomly generated ones are considered.
    ones are considered
    :param conf_path: [str] path to config file (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return [dict] dictionary containing the missing identifiers as keys and the change_type as value
    """
    url = get_url(conf_path, url)
    session = create_session(url)

    # If both Asset IDs from the blockchain and randomly generated once are considered
    if not blockchain_only:
        existing = session.query(distinct(Assets.identifier)) \
            .filter(Assets.identifier.in_(identifiers)).all()
        existing_identifiers = set([identifier[0] for identifier in existing])
        return {i: 'created' for i in identifiers if i not in existing_identifiers}

    # If only Asset IDs which come from the blockchain should be considered
    blockchain = session.query(Assets.identifier, func.count(Assets.identifier).label('b_count')) \
        .filter(Assets.identifier.in_(identifiers)) \
        .group_by(Assets.identifier) \
        .subquery('blockchain')
    existing = session.query(Assets.identifier, Assets.from_blockchain) \
        .filter(Assets.identifier.in_(identifiers)) \
        .filter(or_(blockchain.c.b_count == 1, and_(blockchain.c.b_count == 2, Assets.from_blockchain))) \
        .all()

    existing_dict = {e[0]: {'from_blockchain': e[1]} for e in existing}
    missing = {}
    for i in identifiers:
        if i in existing_dict.keys() and not existing_dict[i]['from_blockchain']:
            missing[i] = 'updated'
        elif i not in existing_dict.keys():
            missing[i] = 'created'
    return missing


def get_synced_dates(conf_path: str=None, url: str=None) -> list:
    """
    Get list of completely synced dates.
    :param conf_path: [str] path to config file (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return: [list of dates] list of existing dates in Sync table
    """
    url = get_url(conf_path, url)
    session = create_session(url)
    sync_dates = session.query(Sync.date_complete).all()
    _logger.debug('number of synced dates: %d' % len(sync_dates))
    return [d[0] for d in sync_dates]


def get_resources_info(req_date: date, conf_path: str=None, url: str=None) -> tuple:
    """
    Get identifier, date_modified and change_type for all records with modification date recq_date.
    :param req_date: [date] modification date for which records should be retrieved
    :param conf_path: [str] path to config file (optional, only needed if url is not specified)
    :param url:[str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return:[tuple] tuple of identifier, date_modified, change_type tuples
    """
    url = get_url(conf_path, url)
    session = create_session(url)
    return session.query(Assets.identifier, Assets.date_modified, Assets.change_type, Assets.asset_id) \
        .filter(Assets.date_modified == req_date).all()


def get_info_from_asset_id(asset_id: str, conf_path: str=None, url: str=None):
    """
    Get identifier and datasetPUID for given Asset-ID.
    :param asset_id: [str] Asset-ID to get additional information for
    :param conf_path: [str] path to config file (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return:
    """
    url = get_url(conf_path, url)
    session = create_session(url)
    return session.query(Assets.identifier, Assets.dataset_puid).filter(Assets.asset_id == asset_id).one_or_none()


if __name__ == '__main__':
    get_synced_dates()
