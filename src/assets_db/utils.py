import os
import uuid
import logging
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime

from assets_db.configuration import load_config
from assets_db.model import Base

_logger = logging.getLogger(__name__)


def create_db(conf_path: str=None, url: str=None):
    """
    Create assets database.
    :param conf_path: [str] path to a config file (optional)
    :param url: [str] uurl to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    """
    url = get_url(conf_path, url)
    engine = create_engine(url)
    Base.metadata.create_all(bind=engine)
    _logger.info('AssetsDB created')


def create_session(url: str):
    """
    Create database session.
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db
    :return: database session
    """
    engine = create_engine(url)
    session = sessionmaker(bind=engine)
    return session()


def get_url(conf_path: str=None, url: str=None):
    """
    Get url from parameter or config file.
    :param conf_path: [str] path to a config file
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    :return: [str] url to database
    """
    if url is not None:
        _logger.debug('Url param used to set url')
    elif os.environ.get('DMA_ASSETS_DB_URL') is not None:
        url = os.environ['DMA_ASSETS_DB_URL']
    else:
        load_config(path=conf_path, keys=['DMA_ASSETS_DB_URL'])
        _logger.debug('Supplied config file used to retrieve url')
        url = os.environ['DMA_ASSETS_DB_URL']
    _logger.debug('DB url set to %s' % url)
    return url


class MetadataAsset:
    def __init__(self, identifier: str, date_modified: datetime, change_type: bool=None):
        self.identifier = identifier
        self.dateModified = date_modified

        if change_type is None:
            change_type = 'created'
        self.changeType = change_type

        # to be filled from blockchain
        self.datasetPUID = None
        self.assetID = None
        self.fromBlockchain = None

    def set_asset(self, asset_id: str, from_blockchain: bool, dataset_puid: str=None):
        if dataset_puid is None:
            dataset_puid = str(uuid.uuid4())
        self.datasetPUID = dataset_puid
        self.assetID = asset_id
        self.fromBlockchain = from_blockchain
