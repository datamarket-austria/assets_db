import logging

from assets_db.model import Assets, Sync
from assets_db.utils import create_session, get_url

_logger = logging.getLogger(__name__)


def insert_dicts(rec_dicts: list, conf_path: str=None, url: str=None):
    """
    Insert a list of dicts into the database.
    :param rec_dicts: [list of dicts] list of record dicts to be inserted
    :param conf_path: [str] path to a config file to get the database url from (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    """
    url = get_url(conf_path, url)
    session = create_session(url)
    for rec in rec_dicts:
        session.add(Assets(**rec))
        session.commit()
    session.close()


def insert_metadata_asset(md: list, conf_path: str=None, url: str=None):
    """
    Insert a list of MetadataAsset.
    :param md: [list] list of MetadataAsset objects
    :param conf_path: [str] path to a config file to get the database url from (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    """
    url = get_url(conf_path, url)
    session = create_session(url)
    for m in md:
        rec = Assets(identifier=m.identifier,
                     dataset_puid=str(m.datasetPUID),
                     asset_id=m.assetID,
                     from_blockchain=m.fromBlockchain,
                     date_modified=m.dateModified,
                     change_type=m.changeType
                     )
        session.add(rec)
        session.commit()
    session.close()


def insert_sync_dates(insert_dates, conf_path: str=None, url: str=None):
    """
    Insert list of dates into Sync table.
    :param insert_dates: [list or date] list of dates or a single date to be inserted into Sync table
    :param conf_path: [str] path to a config file to get the database url from (optional, only needed if url is not specified)
    :param url: [str] url to a database, e.g.: sqlite:///path/to/database/file.db (optional)
    """
    url = get_url(conf_path, url)
    session = create_session(url)

    if type(insert_dates) != list:
        insert_dates = [insert_dates]

    for insert_date in insert_dates:
        session.add(Sync(date_complete=insert_date))
    session.commit()
    session.close()


if __name__ == '__main__':
    pass
