import os
import configparser


def load_config(path: str=None, keys: list=None):
    """
    Load requested keys from config file.
    :param path: [str] path to config file
    :param keys: [list] list of keys to be loaded from config file
    """

    mapping = {
        'DMA_ASSETS_DB_URL': ['assets_db', 'url']
    }

    if keys is None:
        keys = mapping.keys()

    # Only load not set keys
    missing_keys = []
    for key in keys:
        if os.environ.get(key) is None:
            missing_keys.append(key)
    if len(missing_keys) == 0:
        return

    path = get_config_path(path)
    config = configparser.ConfigParser()
    config.read(path)

    for key in missing_keys:
        val = mapping[key]
        os.environ[key] = config.get(val[0], val[1])  # raise Error if val does not exist


def get_config_path(path: str=None) -> str:
    """
    Get path to config file.
    Checks given path, env variable, home and /etc folder
    :param path: [str] path to config file (optional)
    :return: [path] path to config file or None
    """
    if path is not None and os.path.isfile(path):
        return path
    if os.environ.get('DMA_ASSETS_DB_CONFIG_PATH') is not None \
            and os.path.isfile(os.environ['DMA_ASSETS_DB_CONFIG_PATH']):
        return os.environ['DMA_ASSETS_DB_CONFIG_PATH']

    folder = 'dma'
    filename = 'assets_db.cfg'
    home_file = os.path.expanduser('~/.%s/%s' % (folder, filename))
    etc_file = '/etc/.%s/%s' % (folder, filename)
    if os.path.isfile(home_file):
        return home_file
    if os.path.isfile(etc_file):
        return etc_file
    raise FileNotFoundError

