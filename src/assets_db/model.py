import logging
from sqlalchemy import Column, Integer, String, Boolean, DateTime, Date, Enum, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base


_logger = logging.getLogger(__name__)
ChangeTypeEnum = Enum('created', 'updated', 'deleted')
Base = declarative_base()


class Assets(Base):
    """
    Table description for Assets table.
    """
    __tablename__ = 'assets'

    id = Column(Integer, primary_key=True)
    identifier = Column(String, index=True, nullable=False)
    dataset_puid = Column(String, unique=True, nullable=False)
    asset_id = Column(String, unique=True, nullable=False)
    from_blockchain = Column(Boolean, nullable=False)
    date_modified = Column(DateTime, nullable=False)
    change_type = Column(ChangeTypeEnum, default='created')

    # Combi identifier from_blockchain should be unique -> equivalent to unique AssetId
    UniqueConstraint('identifier', 'from_blockchain', 'uq_identifier_blockchain')


class Sync(Base):
    """
    Table to see which dates have been added completely to Assets table.
    """
    __tablename__ = 'sync'

    id = Column(Integer, primary_key=True)
    date_complete = Column(Date, unique=True, nullable=False)
