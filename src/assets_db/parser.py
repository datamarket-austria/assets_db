import sys
import argparse
import logging
from datetime import datetime

from assets_db.utils import create_db
from assets_db.insert import insert_sync_dates

_logger = logging.getLogger(__name__)


def parse_args(args):
    """
    Parse commandline parameters.
    :param args: command line parameters as list of strings
    :return: [obj:`argparse.Namespace`] command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description='Script to handle assets database.'
    )
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO
    )
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG
    )
    parser.add_argument(
        '-u',
        '--db_url',
        dest='db_url',
        help='Url where the database should be created (optional), e.g.: sqlite:////path/to/database/file.db'
    )
    parser.add_argument(
        '-c',
        '--config',
        dest='config',
        help='Path to config file (database url needs to be included)'
    )

    subparser = parser.add_subparsers(
        dest='subparser_name',
        description="Commands to use with assets_db"
    )
    subparser.add_parser(
        'create_db',
        help='Script to create AssetsDB'
    )
    insert_sync_parser = subparser.add_parser(
        'insert_sync',
        help='Script to insert dates into Sync table'
    )
    insert_sync_parser.add_argument(
        dest='insert_dates',
        help='List of dates (%Y-%m-%d) to insert into Sync tables separated with "," e.g.: 2019-01-01,2019-01-02'
    )

    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging
    :param loglevel: [int] minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel,
        stream=sys.stdout,
        format=logformat,
        datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls
    :param args: [list] command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)

    if args.subparser_name == 'create_db':
        _logger.debug('Start creating assets database ...')
        create_db(conf_path=args.config, url=args.db_url)
        _logger.info('Assets database created')

    elif args.subparser_name == 'insert_sync':
        insert_str = args.insert_dates.split(',')
        insert_dates = [datetime.strptime(i, '%Y-%m-%d') for i in insert_str]
        _logger.debug('Start inserting %d date(s).' % len(insert_dates))
        insert_sync_dates(insert_dates=insert_dates, conf_path=args.config, url=args.db_url)
        _logger.info('Finished inserting %d date(s) into Sync table.' % len(insert_dates))


def run():
    """Entry point for console_scripts"""
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
