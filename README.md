# assetsDB

A simple database to store matching between local identifier and Asset-IDs.

## Description

This package is used by the [assetsRetrieval](https://gitlab.com/datamarket-austria/assets_retrieval) and
[externalNodeEndpoint](https://gitlab.com/datamarket-austria/external_node_endpoint) package to store the matching between
local identifier (used by the external node) and DMA Asset-ID. If only used in combination with these packages there is
no need to install the package itself.

## Installation

1. Clone this repository (using ssh in this case) and navigate into the folder
    ```bash
    git clone git@gitlab.com:datamarket/assets_db.git
    cd /path/to/assets_db
    ```
1. Create/Modify settings file
    ```bash
    mkdir ~/.dma
    cp settings_sample.cfg ~/.dma/assets_db.cfg
    ```
    Inside the new assets_db.cfg file edit der url parameter to a location where the database should be stored

1. Recommended: Create a virtual environment and activate it, tested with Python 3.6
    ```bash
    virtualenv assets_db -p python3.6
    source assets_db/bin/activate
    ```
1. Install package (Make sure you are inside the assets_db folder)
    ```bash
    cd /path/to/assets_db
    python setup.py install
    ```
    Replace ``install`` with ``develop`` if you plan to modify code


## Usage

When the package is installed a console script is added:

``assets_db``: This is the wrapper for all processes that can be run.
Currently there are two subcommands that can be called with ``assets_db``:

- ``create_db``: to create an AssetsDB
- ``insert_sync``: to insert a list of dates into the Sync table

Please use ``--help`` to get a complete overview of possible parameters

## Changes for different external nodes

No changes are needed to be used with different metadata providers


## Note

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
